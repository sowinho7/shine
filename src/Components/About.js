import React, { Component } from "react";
import stamp from "./stam.png";
import one from "../images/1.png";
import two from "../images/2.png";
import three from "../images/3.png";
import four from "../images/4.png";
import five from "../images/5.png";
import six from "../images/6.png";

class About extends Component {
  render() {
    return (
      <section id="about">
        <div className="row">
          <div className="three columns">
            {/* <img
              className="profile-pic"
              src={profilepic}
              alt="Nordic Giant Profile Pic"
            /> */}
            <img alt="stamp" src={stamp} />
            {/* <span className="block-example">Holowanie 24/7</span> */}
          </div>
          <div className="nine columns main-col">
            <h1>Nasza specjalizacja:</h1>
            <br />

            <div>
              <img alt="one" class="numbers" src={one} />
              Likwidacja szkód komunikacyjnych i przywracaniu pojazdu do stanu
              sprzed kolizji.
            </div>
            <div>
              <img alt="two" class="numbers" src={two} />
              Dokonujemy napraw blacharsko - lakierniczych, mechanicznych
              pojazdów osobowych i dostawczych.
            </div>
            <div>
              <img alt="three" class="numbers" src={three} />
              Prowadzimy wypożyczalnię pojazdów zastępczych na okres likwidacji
              szkody i naprawy pojazdu.
            </div>
            <div>
              <img alt="four" class="numbers" src={four} />
              Nasze lawety są do Państwa dyspozycji 24 godziny na dobę, 7 dni w
              tygodniu.
            </div>
            <div>
              <img alt="five" class="numbers" src={five} /> Wyróżniają nas
              indywidualne podejście do każdego zlecenia i niskie ceny.
            </div>
            <div>
              <img alt="six" class="numbers" src={six} />W przypadku klientów,
              którzy mieli kolizję, chcemy zapewnić im jak najlepsze warunki na
              czas likwidacji szkody komunikacyjnej i naprawy pojazdu. W tym
              celu załatwiamy za klienta wszystkie formalności z
              ubezpieczycielem, dajemy auto zastępcze i dokładnie weryfikujemy
              każde uszkodzenia pojazdu, tak żeby na samochodzie nie został
              żaden ślad po wypadku.
            </div>
            {/* <div className="row">
              <div className="columns contact-details">
                <h2>Contact Details</h2>
                <p className="address">
                  <span>{name}</span>
                  <br />
                  <span>
                    {street}
                    <br />
                    {city} {state}, {zip}
                  </span>
                  <br />
                  <span>{phone}</span>
                  <br />
                  <span>{email}</span>
                </p>
              </div>
              <div className="columns download">
                <p>
                  <a href={resumeDownload} className="button">
                    <i className="fa fa-download"></i>Download Resume
                  </a>
                </p>
              </div>
            </div> */}
          </div>
        </div>
      </section>
    );
  }
}

export default About;
