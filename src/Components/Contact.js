import React, { Component } from "react";
import contactdata from "../images/znak.png";

class Contact extends Component {
  render() {
    return (
      <section id="contact">
        <div className="row">
          <div className="eight columns">
            <div class="map-responsive">
              <iframe
                title="map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2438.4586614546874!2d21.11676121580028!3d52.32582487977781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3866790455783a3f!2sShine%20of%20Divine%20Warszawa!5e0!3m2!1spl!2spl!4v1602001760993!5m2!1spl!2spl"
                width={300}
                height={350}
                frameborder={0}
                style={{ border: 0 }}
                allowfullscreen={false}
                aria-hidden={false}
                tabindex={0}
              ></iframe>
            </div>
          </div>

          <div className="four columns">
            <img alt="contactdata" className="znak" src={contactdata} />
          </div>
        </div>
      </section>
    );
  }
}

export default Contact;
