import React, { Component } from "react";

class Holowanie extends Component {
  render() {
    return (
      <section id="holowanie">
        <div className="row education">
          <div className="three columns header-col">
            <h1>
              <span>Holowanie</span>
            </h1>
          </div>

          <div className="nine columns main-col">
            <div className="row item">
              <div className="twelve columns">
                <p className="info">
                  Oferujemy usługę holowania pojazdu na lawecie z lub do
                  dowolnego miejsca. Ceny ustalamy indywidualnie telefonicznie
                  lub osobiście. Jeśli są Państwo poszkodowanymi w kolizji
                  drogowej, koszt holowania pokrywa ubezpieczenia sprawcy więc
                  nie ponoszą Państwo kosztów z nim związanych. W sprawie
                  holowania za granicę Polski, więcej informacji udzielamy
                  telefonicznie.
                </p>
                <p className="info">
                  Nasz teren działania to: Marki, Warszawa, Ząbki, Zielonka,
                  Kobyłka, Wołomin, Nadma, Słupno, Radzymin, Trasa Toruńska.
                </p>
                <p className="info">
                  Nasz drugi rejon to: Wyszków, Łochów i okolice, trasa S8 z
                  Marek do Wyszkowa
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Holowanie;
