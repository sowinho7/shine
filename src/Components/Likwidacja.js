import React, { Component } from "react";
import tick from "../images/tick.png";

class Likwidacja extends Component {
  render() {
    return (
      <section id="likwidacja">
        <div className="row education">
          <div className="three columns header-col">
            <h1>
              <span>Likwidacja szkód</span>
            </h1>
          </div>

          <div className="nine columns main-col">
            <div className="row item">
              <div className="twelve columns">
                <h3>Prowadzimy pełną likwidację szkody za klienta</h3>
                <div className="info">
                  <img alt="tick" class="tick" src={tick} />
                  Oferujemy holowanie pojazdu z miejsca kolizji lub parkingu, na
                  którym stoi.
                </div>
                <div className="info">
                  <img alt="tick" class="tick" src={tick} />
                  Pojazd stoi na naszym strzeżonym parkingu zabezpieczonym w
                  dwie bramy wjazdowe, kamery i oświetlenie reagujące na ruch.
                </div>
                <div className="info">
                  <img alt="tick" class="tick" src={tick} />W przypadku szkody
                  nie z winy klienta, oferujemy pojazd zastępczy na cały okres
                  likwidacji szkody lub cały okres naprawy. Koszt auta
                  zastępczego pokrywany jest przez ubezpieczalnię sprawcy, więc
                  nie ponoszą Państwo kosztów związanych z wynajem. Następnie
                  zgłaszamy szkodę do ubezpieczalni i przeprowadzamy oględziny
                  pojazdu z pełnym demontażem w uszkodzonej strefie, tak żeby
                  wykryć każde uszkodzenie. W międzyczasie zalatwimy za Państwa
                  wszelkie formalności związane ze szkodą, czyli dostarczamy
                  wymagane dokumenty, wyliczamy realną wartość szkody oraz
                  monitorujemy przebieg likwidacji przez ubezpieczalnię.
                  Rozpoczynamy naprawę pojazdu. Auta naprawiane są na możliwie
                  najlepszych jakościowo częściach oraz za pomocą nowoczesnego
                  sprzętu. Do każdej naprawy podchodzimy indywidualnie.
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Likwidacja;
