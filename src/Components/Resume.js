import React, { Component } from "react";

class Resume extends Component {
  render() {
    return (
      <section id="warsztat">
        <div className="row education">
          <div className="three columns header-col">
            <h1>
              <span>Naprawy blacharsko-lakiernicze</span>
            </h1>
          </div>

          <div className="nine columns main-col">
            <div className="row item">
              <div className="twelve columns">
                <p className="info">
                  Nasz warsztat blacharsko - lakierniczy jest do Państwa
                  dyspozycji od poniedziałku do piątku. Bazujemy na naprawach
                  bezgotówkowych, których koszt pokrywa ubezpieczyciel sprawcy
                  kolizji lub ubezpieczenie Autocasco Naprawy odbywają się wedle
                  zaleceń wyceny eksperckiej na częściach oryginalnych.
                  Posiadamy nowoczesny sprzęt, za pomocą którego jesteśmy w
                  stanie naprawić każdy pojazd. W przypadku napraw gotówkowych
                  zapraszamy na wycenę naprawy, którą można uzyskać od ręki po
                  wcześniejszym umówieniu się na konkretny termin.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="row work">
          <div className="three columns header-col">
            <h1>
              <span>Usługi mechaniczne</span>
            </h1>
          </div>

          <div className="nine columns main-col">
            <p className="info">
              Świadczymy usługi mechaniczne zawieszenia pojazdu, diagnostykę
              komputerową pojazdu, układu hamulcowego, kierowniczego i
              napędowego. Oferujemy również naprawy okresowe związane z
              eksploatacją pojazdu takie jak: wymiana oleju i filtrów, wymianę
              opon, rozrządu, czy klocków hamulcowych. Jesteśmy w stanie
              wyliczyć od ręki przybliżony koszt naprawy.
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default Resume;
