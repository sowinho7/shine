import React, { Component } from "react";
import main1 from "../images/main3.jpg";
import main2 from "../images/main2.jpg";

class Header extends Component {
  render() {
    return (
      <header id="home">
        <img
          className="imgmain"
          src={main1}
          srcSet={`${main2} 300w, ${main2} 768w,${main1} 1280w`}
          sizes="(min-width: 800px) 70vw, 100vw"
          alt="shineofdivine"
        />
        {/* <img className="imgmain" src={wyn} alt="qwe" /> */}
        <nav id="nav-wrap">
          <a className="mobile-btn" href="#nav-wrap" title="Pokaz menu">
            Pokaż menu
          </a>
          <a className="mobile-btn" href="#home" title="Ukryj Menu">
            Ukryj menu
          </a>

          <ul id="nav" className="nav">
            <li className="current">
              <a className="smoothscroll" href="#home">
                Home
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#about">
                O nas
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#warsztat">
                Warsztat
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#likwidacja">
                Likwidacja szkód
              </a>
            </li>
            {/* <li>
              <a className="smoothscroll" href="#warsztat">
                Warsztat
              </a>
            </li> */}
            <li>
              <a className="smoothscroll" href="#holowanie">
                Holowanie
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#contact">
                Kontakt
              </a>
            </li>
          </ul>
        </nav>
        <div id="main-img">
          <div className="row banner">
            <div className="banner-text">
              {/* <h1 className="responsive-headline">{name}</h1>
            <h3>QWEQWEWQ</h3> */}
              <hr />
            </div>
          </div>
        </div>
        <p className="scrolldown">
          <a className="smoothscroll" href="#about">
            <i className="icon-down-circle"></i>
          </a>
        </p>
      </header>
    );
  }
}

export default Header;
